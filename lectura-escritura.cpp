#include <iostream>
#include <fstream>

using namespace std;

string rutaBase = "/Users/maxvega/CLionProjects/clasesprogramacion/";

void fstream() {
    fstream archivo(std::fstream::in | std::fstream::out);
    archivo.open(rutaBase + "ejemplo.txt");
    if (archivo.is_open()) {
        archivo << "Hola! esto está en un archivo" << endl;
        archivo.close();
    } else {
        cout << "No puedo abrir el archivo, verifique permisos y espacio en el disco" << endl;
    }
    string linea;
    if (archivo.is_open()) {
        while (getline(archivo, linea)) {
            cout << linea << endl;
        }
        archivo.close();
    }
}

void escribirArchivo() {
    ofstream archivo;
    archivo.open(rutaBase + "ejemplo.txt");
    if (archivo.is_open()) {
        archivo << "Hola! esto está en un archivo" << endl;
        archivo.close();
    } else {
        cout << "No puedo abrir el archivo, verifique permisos y espacio en el disco" << endl;
    }
    archivo.open(rutaBase + "ejemplo2.txt");
    if (archivo.is_open()) {
        archivo << "Hola! esto está en un archivo" << endl;
        archivo.close();
    } else {
        cout << "No puedo abrir el archivo, verifique permisos y espacio en el disco" << endl;
    }
}

void leerArchivo() {
    string linea;
    ifstream archivo(rutaBase + "ejemplo.txt");
    if (archivo.is_open()) {
        while (getline(archivo, linea)) {
            cout << linea << endl;
        }
        archivo.close();
    }
}

void escribirArchivoAlFinal() {
    ofstream archivo(rutaBase + "ejemplo.txt", ios::app); // append
    if (archivo.is_open()) {
        archivo << "Escribiendo en un archivo al final" << endl;
        archivo.close();
    }
}

int main() {
    escribirArchivo();
    leerArchivo();
    escribirArchivoAlFinal();
    leerArchivo();
    return 0;
}