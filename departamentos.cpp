#include <iostream>

using namespace std;

/*
 * Diseñe un programa que permita la venta de departamentos, en distintos edificios y distintos pisos utilizando arreglos multi dimensionales.
 */

void inicializar(int *edificios[], int cantidadEdificios, int cantidadPisos[], int departamentos[]) {
    for (int i = 0; i < cantidadEdificios; i++) {
        int *pisos[cantidadPisos[i]];
        for (int j = 0; j < cantidadPisos[i]; j++) {
            pisos[j] = new int[departamentos[i]];
            for (int z = 0; z < departamentos[i]; z++) {
                pisos[j][z] = 0;
            }
        }
        edificios[i] = pisos[i];
    }
}

void imprimirEdificio(int *edificio, int cantidadPisos[], int indiceEdificio, int departamentos[]) {
    for (int j = 0; j < cantidadPisos[indiceEdificio]; j++) {
        cout << "Piso " << j + 1 << "\t|";
        int *pisos = &edificio[j];
        for (int z = 0; z < departamentos[indiceEdificio]; z++) {
            cout << "|\t\t" << "Dpto " << *pisos + 1 << z + 1 << ((pisos[z] == 0) ? " Disponible" : " Ocupado   ")
                 << "\t\t|";
        }
        cout << endl;
    }
    cout << "-----------------------------------------------------------------" << endl;
}

void imprimirTodos(int *edificios[], int cantidadEdificios, int cantidadPisos[], int departamentos[]) {
    for (int i = 0; i < cantidadEdificios; i++) {
        cout << "Edificio " << i + 1 << endl;
        cout << "-----------------------------------------------------------------" << endl;
        imprimirEdificio(edificios[i], cantidadPisos, i, departamentos);
    }
}

int *ResumenEdificio(int *edificio, int cantidadPisos[], int indiceEdificio, int departamentos[]) {
    int *acumulador = new int[2];
    acumulador[0] = 0;
    acumulador[1] = 0;
    int cantidadOcupada = 0, cantidadDisponible = 0;
    for (int j = 0; j < cantidadPisos[indiceEdificio]; j++) {
        int *pisos = &edificio[j];
        for (int z = 0; z < departamentos[indiceEdificio]; z++) {
            if (pisos[z] == 0) {
                acumulador[0]++;
            } else {
                acumulador[1]++;
            }
        }
    }

    return acumulador;
}

bool venderDepartamento(int *edificios[], int cantidadEdificios, int cantidadPisos[], int departamentos[],
                        int departamentoSeleccionado, int pisoSeleccionado, int indiceEdificio) {
    if (indiceEdificio > cantidadEdificios
        || pisoSeleccionado > cantidadPisos[indiceEdificio]
        || departamentoSeleccionado > departamentos[indiceEdificio]) {
        return false;
    }
    int *edificio = edificios[indiceEdificio];
    int *pisos = &edificio[pisoSeleccionado];
    cout << pisos[departamentoSeleccionado] << endl;
    if (pisos[departamentoSeleccionado] == 0) {
        pisos[departamentoSeleccionado] = 1;
        return true;
    }
    return false;
}


void imprimirResumen(int *edificios[], int cantidadEdificios, int cantidadPisos[], int departamentos[]) {
    int cantidadTotalDisponible = 0;
    int cantidadTotalOcupada = 0;
    for (int i = 0; i < cantidadEdificios; i++) {
        cout << "Edificio " << i + 1 << endl;
        cout << "-----------------------------------------------------------------" << endl;
        int *resumen = ResumenEdificio(edificios[i], cantidadPisos, i, departamentos);
        cout << "Disponible: " << resumen[0] << " \t\t " << "Ocupada: " << resumen[1] << endl;
        cout << "-----------------------------------------------------------------" << endl;
        cantidadTotalDisponible += resumen[0];
        cantidadTotalOcupada += resumen[1];
    }
    cout << "Total disponible: " << cantidadTotalDisponible << " \t\t " << "Total Ocupado: " << cantidadTotalOcupada
         << endl;
}

int main() {
    int cantidadEdificios;
    cout << "Ingrese cantidad de edificios" << endl;
    cin >> cantidadEdificios;
    int cantidadPisos[cantidadEdificios];
    int departamentos[cantidadEdificios];
    for (int i = 0; i < cantidadEdificios; i++) {
        cout << "Cuantos pisos tiene el edificio " << i + 1 << endl;
        cin >> cantidadPisos[i];
        cout << "Cuantos departamentos por piso hay en el edificio " << i + 1 << endl;
        cin >> departamentos[i];
    }

    int *edificios[cantidadEdificios];

    inicializar(edificios, cantidadEdificios, cantidadPisos, departamentos);

    imprimirTodos(edificios, cantidadEdificios, cantidadPisos, departamentos);

    int opcion, indiceEdificio;
    while (true) {
        cout << "Gestor inmobiliario" << endl;
        cout << "1. Disponibilidad por edificio" << endl;
        cout << "2. Disponibilidad de todos los edificios" << endl;
        cout << "3. Resumen venta y disponibles" << endl;
        cout << "4. Venta de departamento" << endl;
        cout << "0. Salir" << endl;
        cout << "Ingrese opción" << endl;
        cin >> opcion;
        if (opcion == 1) {
            cout << "Ingrese índice de edificio: [Rango de opciones " << 0 << " ... " << cantidadEdificios - 1 << "]"
                 << endl;
            cin >> indiceEdificio;
            imprimirEdificio(edificios[indiceEdificio], cantidadPisos, indiceEdificio, departamentos);
        } else if (opcion == 2) {
            imprimirTodos(edificios, cantidadEdificios, cantidadPisos, departamentos);
        } else if (opcion == 3) {
            imprimirResumen(edificios, cantidadEdificios, cantidadPisos, departamentos);
        } else if (opcion == 4) {
            int pisoSeleccionado, departamentoSeleccionado;
            cout << "Ingrese índice de edificio: [Rango de opciones " << 0 << " ... " << cantidadEdificios - 1 << "]"
                 << endl;
            cin >> indiceEdificio;
            cout << "Ingrese el piso del edificio: [Rango de opciones " << 0 << " ... "
                 << cantidadPisos[indiceEdificio] - 1 << "]"
                 << endl;
            cin >> pisoSeleccionado;
            cout << "Ingrese el departamento del piso: [Rango de opciones " << 0 << " ... "
                 << departamentos[indiceEdificio] - 1 << "]"
                 << endl;
            cin >> departamentoSeleccionado;
            if (venderDepartamento(edificios, cantidadEdificios, cantidadPisos, departamentos, departamentoSeleccionado,
                                   pisoSeleccionado, indiceEdificio)) {
                cout << "Vendido con éxito" << endl;
            } else {
                cout << "No está disponible para la venta" << endl;
            }
        } else if (opcion == 0) {
            break;
        }
    }
    return 0;
}