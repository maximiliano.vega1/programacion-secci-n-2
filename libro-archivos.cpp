#include <iostream>
#include <cstring>
#include <fstream>

using namespace std;
string rutaBase = "/Users/maxvega/CLionProjects/clasesprogramacion/biblioteca/";

/*
 * Un libro tiene texto, palabras que están separadas por espacio.
 * Las páginas son un puntero hacia un arreglo de palabras
 * Y el libro tiene un arreglo de páginas
 *
 * Cree un programa que permita administrar y leer un libro.
 *
 */


int main() {
    string ingreso, linea;
    int opcion;
    while (true) {
        cout << "---------------------" << endl;
        cout << "Sistema de biblioteca" << endl;
        cout << "1. Leer libro" << endl;
        cout << "2. Escribir en libro" << endl;
        cout << "3. Buscar en libro" << endl;
        cout << "4. Libros disponible" << endl;
        cout << "0. Salir" << endl;
        cin >> opcion;
        cout << endl << "---------------------" << endl;
        if (opcion == 1) {
            cout << "Que libro deseas leer?" << endl;
            cin.ignore();
            getline(cin, ingreso);
            ifstream fichero(rutaBase + ingreso + ".txt");
            if (fichero.is_open()) {
                int pagina = 1;
                cout << "Leyendo página: " << pagina << endl;
                cout << "---------------------------------------" << endl;
                while (getline(fichero, linea)) {
                    cout << linea << endl;
                    if(linea == "") {
                        pagina++;
                        cout << "Quiere continuar leyendo? Si para continuar" << endl;
                        getline(cin, ingreso);
                        if(ingreso == "Si" || ingreso == "si") {
                            cout << "Leyendo página: " << pagina << endl;
                            cout << "---------------------------------------" << endl;
                            continue;
                        } else {
                            break;
                        }
                    }
                }
            } else {
                cout << "Libro no encontrado o hay problemas de permisos" << endl;
            }
        } else if (opcion == 2) {

        } else if (opcion == 3) {

        } else if (opcion == 4) {
            ifstream fichero(rutaBase + "basededatos.txt");
            if(fichero.is_open()) {
                while(getline(fichero, linea)) {
                    cout << linea << endl;
                }
            }
        } else if (opcion == 0) {
            break;
        } else {
            cout << "Opción invalida" << endl;
        }
    }
    cout << "Adios :D" << endl;
    return 0;
}