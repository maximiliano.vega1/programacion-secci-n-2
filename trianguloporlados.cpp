#include <iostream>

using namespace std;

int main() {
    int a, b, c;
    cout << "Ingrese lados del triangulo" << endl;

    cin >> a >> b >> c;

    if (a + b > c && a + c > b && b + c > a && a > 0 && b > 0 && c > 0) {
        cout << "Valido" << endl;
    } else {
        cout << "Invalido" << endl;
    }
    return 0;
}
