#include <iostream>

using namespace std;
/* Promedio

        Escriba un programa que calcule el promedio de 4 notas ingresadas por el usuario:
Primera nota: 55
Segunda nota: 71
Tercera nota: 46
Cuarta nota: 87
El promedio es: 64.75
*/

int main() {
    float n1, n2, n3, n4;

    cout.precision(3);

    cout << "Primera nota" << endl;
    cin >> n1;
    cout << "Segunda nota" << endl;
    cin >> n2;
    cout << "Tercera nota" << endl;
    cin >> n3;
    cout << "Cuarta nota" << endl;
    cin >> n4;

    float suma = n1 + n2 + n3 + n4;

    float promedio = suma/4;

    cout << "El promedio es: " << promedio << endl;
    return 0;
}
