#include <iostream>

using namespace std;

/*
 * El usuario ingresará una cantidad de números, y debemos ordenar el arreglo de menor a mayor
 */

void imprimirArreglo(int arreglo[], int largo) {

    cout << "Valor:\t";

    for (int i = 0; i < largo; i++) {
        cout << arreglo[i] << "\t";
    }
    cout << endl;
    cout << "Indice:\t";

    for (int i = 0; i < largo; i++) {
        cout << i << "\t";
    }
    cout << endl;
}

void ordenar(int arreglo[], int largo, int inverso = 0) {
    int tmp;
    for (int i = 0; i < largo - 1; i++) {
        for (int j = i + 1; j < largo; j++) {
            if (inverso == 0) {
                if (arreglo[j] < arreglo[i]) {
                    tmp = arreglo[i];
                    arreglo[i] = arreglo[j];
                    arreglo[j] = tmp;
                }
            } else {
                if (arreglo[j] > arreglo[i]) {
                    tmp = arreglo[i];
                    arreglo[i] = arreglo[j];
                    arreglo[j] = tmp;
                }
            }
        }
    }
}

int numeros[5] = {57, 3, -5, 11, 2};

int main() {
    cout << "Arreglo actual" << endl;
    imprimirArreglo(numeros, 5);
    cout << endl;

    cout << "Ordenar de menor a mayor" << endl;
    ordenar(numeros, 5);
    imprimirArreglo(numeros, 5);
    cout << endl;

    cout << "Ordenar de mayor a menor" << endl;
    ordenar(numeros, 5, 1);
    imprimirArreglo(numeros, 5);

    return 0;
}