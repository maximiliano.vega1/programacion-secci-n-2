#include <iostream>
#include <cstring>

using namespace std;

/*
 * Un libro tiene texto, palabras que están separadas por espacio.
 * Las páginas son un puntero hacia un arreglo de palabras
 * Y el libro tiene un arreglo de páginas
 *
 * Cree un programa que permita administrar y leer un libro.
 *
 */

void inicializar(string *biblioteca[], int cantidadLibros) {
    string *libroAuxiliar;
    int cantidadPaginas;
    for (int i = 0; i < cantidadLibros; i++) {
        cout << "Libro " << i + 1 << endl;
        cout << "Cantidad de paginas" << endl;
        cin >> cantidadPaginas;

        cin.ignore();

        biblioteca[i] = new string[cantidadPaginas + 1];

        libroAuxiliar = biblioteca[i];

        cout << "Ingrese nombre del libro" << endl;

        getline(cin, libroAuxiliar[0]);
    }
}

string *buscarPorNombre(string busqueda, string *biblioteca[], int cantidadLibros) {
    string *libroAuxiliar;
    for (int i = 0; i < cantidadLibros; i++) {
        libroAuxiliar = biblioteca[i];
        if (libroAuxiliar[0] == busqueda) {
            return libroAuxiliar;
        }
    }
    return nullptr;
}

string *obtenerPorIndice(int indice, string *biblioteca[], int cantidadLibros) {
    if (indice > cantidadLibros) {
        return nullptr;
    }
    return biblioteca[indice];
}

string entradaDeParrafos() {
    string ingreso;
    string retorno = "";
    cin.ignore();
    while (true) {
        cout << "Ingrese texto para la página, presione enter sin texto para terminar" << endl;
        getline(cin, ingreso);
        if (ingreso.empty()) {
            break;
        }
        retorno += ingreso + "\n";
    }
    return retorno;
}


void menu(string *libroSeleccionado, string *biblioteca[], int cantidadLibros) {
    int opcion, pagina;
    string ingreso;
    while (true) {
        cout << "----------------------------------------------" << endl;
        if (libroSeleccionado != nullptr) {
            cout << "Libro actual: " << libroSeleccionado[0] << endl;
        } else {
            cout << "No hay libro seleccionado actualmente" << endl;
        }
        cout << "----------------------------------------------" << endl
             << "1. Buscar libro" << endl
             << "2. Leer página" << endl
             << "3. Escribir/modificar página" << endl
             << "4. Borrar página" << endl
             << "0. Salir" << endl;
        cin >> opcion;
        if (opcion == 1) {
            cout << "1. Buscar por nombre" << endl;
            cout << "2. Buscar por índice" << endl;
            cin >> opcion;
            if (opcion == 1) {
                cout << "Ingrese nombre del libro" << endl;
                cin.ignore();
                getline(cin, ingreso);

                libroSeleccionado = buscarPorNombre(ingreso, biblioteca, cantidadLibros);

                if (libroSeleccionado == nullptr) {
                    cout << "No lo encontré" << endl;
                }
            } else if (opcion == 2) {
                cout << "Ingrese el índice del libro" << endl;
                cin >> opcion;
                libroSeleccionado = obtenerPorIndice(opcion, biblioteca, cantidadLibros);
                if (libroSeleccionado == nullptr) {
                    cout << "No lo encontré" << endl;
                } else {
                    cout << "Libro encontrado: " << libroSeleccionado[0] << endl;
                }
            } else {
                cout << "Opción inválida" << endl;
            }
        } else if (opcion == 2) {
            if (libroSeleccionado == nullptr) {
                cout << "No hay libro seleccionado, use la opcion 1" << endl;
                continue;
            }
            cout << "¿Qué página?" << endl;
            cin >> pagina;
            cout << libroSeleccionado[pagina] << endl;
        } else if (opcion == 3) {
            if (libroSeleccionado == nullptr) {
                cout << "No hay libro seleccionado, use la opcion 1" << endl;
                continue;
            }
            cout << "¿Qué página?" << endl;
            cin >> pagina;
            libroSeleccionado[pagina] = entradaDeParrafos();
        } else if (opcion == 4) {
            if (libroSeleccionado == nullptr) {
                cout << "No hay libro seleccionado, use la opcion 1" << endl;
                continue;
            }
            cout << "¿Qué página?" << endl;
            cin >> pagina;
            libroSeleccionado[pagina] = "";
        } else if (opcion == 0) {
            cout << "Adios" << endl;
            break;
        } else {
            cout << "Opción inválida" << endl;
        }
    }
}


int main() {
    int cantidadLibros;

    cout << "Cantidad de libros" << endl;
    cin >> cantidadLibros;

    string *biblioteca[cantidadLibros];
    inicializar(biblioteca, cantidadLibros);

    string *libroSeleccionado = nullptr;
    menu(libroSeleccionado, biblioteca, cantidadLibros);

    return 0;
}