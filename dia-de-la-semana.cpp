#include <iostream>

using namespace std;

int main() {
    int diaDeLaSemana;

    cout << "Ingrese el numero de dia entre 1 y 7" << endl;
    cin >> diaDeLaSemana;

    if(diaDeLaSemana == 1) {
        cout << "Lunes" << endl;
    } else if (diaDeLaSemana == 2) {
        cout << "Martes" << endl;
    } else if (diaDeLaSemana == 3) {
        cout << "Miercoles" << endl;
    } else if (diaDeLaSemana == 4) {
        cout << "Jueves" << endl;
    } else if (diaDeLaSemana == 5) {
        cout << "Viernes" << endl;
    } else if (diaDeLaSemana == 6) {
        cout << "Sabado" << endl;
    } else if (diaDeLaSemana == 7) {
        cout << "Domingo" << endl;
    } else {
        cout << "Ingresa bien los numeros >_>" << endl;
    }

    return 0;
}